//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/sv/ssv9000005/models"
	"git.forms.io/isaving/sv/ssv9000005/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv9000005Controller struct {
	controllers.CommTCCController
}

func (*Ssv9000005Controller) ControllerName() string {
	return "Ssv9000005Controller"
}

// @Desc ssv9000005 controller
// @Description Entry
// @Param ssv9000005 body models.SSV9000005I true "body for user content"
// @Success 200 {object} models.SSV9000005O
// @router /ssv9000005 [post]
// @Author
// @Date 2020-12-04
func (c *Ssv9000005Controller) Ssv9000005() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv9000005Controller.Ssv9000005 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv9000005I := &models.SSV9000005I{}
	if err := models.UnPackRequest(c.Req.Body, ssv9000005I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv9000005I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv9000005 := &services.Ssv9000005Impl{
	}
	ssv9000005.New(c.CommTCCController)
	ssv9000005.Sv900005I = ssv9000005I
	//ssv9000005.DlsInterface = &commclient.DlsOperate{}
	ssv9000005Compensable := services.Ssv9000005Compensable

	proxy, err := aspect.NewDTSProxy(ssv9000005, ssv9000005Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv9000005I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv9000005O, ok := rsp.(*models.SSV9000005O); ok {
		if responseBody, err := models.PackResponse(ssv9000005O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv9000005 Controller
// @Description ssv9000005 controller
// @Param Ssv9000005 body models.SSV9000005I true body for SSV9000005 content
// @Success 200 {object} models.SSV9000005O
// @router /create [post]
/*func (c *Ssv9000005Controller) SWSsv9000005() {
	//Here is to generate API documentation, no need to implement methods
}
*/
