package dao

import (
	"fmt"
	"git.forms.io/isaving/sv/ssv9000005/models"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
)

type TPersonArgeement struct {
	AgreementId     string
	AgreementType   string
	Currency        string
	CashtranFlag    string
	AgreementStatus string
	FreezeType      string
	TermFlag        string
	DepcreFlag      string
	AccFlag         string
	AccAcount       string
	BegnTxDt        string
	AppntDtFlg      string
	AppntDt         string
	PrductId        string
	PrductNm        int
	CstmrId         string
	CstmrTyp        string
	AccuntNme       string
	UsgCod          string
	WdrwlMthd       string
	AccPsw          string
	PswWrongTime    int
	TrnWdrwmThd     string
	OpnAmt          float64
	AccCnclFlg      string
	AutCnl          string
	DytoCncl        int
	SttmntFlg       string
	WthdrwlMthd     string
	OvrDrwFlg       string
	CstmrCntctAdd   string
	CstmrCntctPh    string
	CstmrCntctEm    string
	BnCrtAcc        string
	BnAcc           string
	AccOpnDt        string
	AccOpnEm        string
	AccCanclDt      interface{}
	AccCanclEm      string
	AccCanclResn    string
	LastUpDatetime  string
	LastUpBn        string
	LastUpEm        string
	TccStatus       string

}

func (t *TPersonArgeement) TableName() string {
	return "t_person_argeement"
}


func QueryToUpdate(ssv9000005I *models.SSV9000005I)(ssv9000005O *models.SSV9000005O, err error){
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.Errorf("SV00000001",  "QueryT_pdsbasp panic:%v " , fmt.Sprintf("%s", e.(interface{})))
		}
	}()

	sql := "SELECT * FROM t_person_argeement where agreement_id=? and agreement_type=? and tcc_status='0'"

	queryInfo, err := EngineCache.QueryString(sql, ssv9000005I.AgreementID,ssv9000005I.AgreementType)
	if err != nil {
		return nil, err
	}
	if len(queryInfo) > 0 {
		return ssv9000005O, nil
	} else {
		err = errors.New("Records not found", "SV99000020")
		return nil, err

	}

}

func UpdateTPersonArgeement(ssv9000005I *models.SSV9000005I)(err error){
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.Errorf("SV00000001",  "QueryT_pdsbasp panic:%v " , fmt.Sprintf("%s", e.(interface{})))
		}
	}()

	sql := "update `t_person_argeement` set accunt_nme=? ,cstmr_cntct_add=? ,cstmr_cntct_ph=? ,cstmr_cntct_em=? where agreement_id=? and agreement_type=?  and tcc_status='0'"
	_, err = EngineCache.Exec(sql,  ssv9000005I.AccuntNme, ssv9000005I.CstmrCntctAdd, ssv9000005I.CstmrCntctPh, ssv9000005I.CstmrCntctEm, ssv9000005I.AgreementID,ssv9000005I.AgreementType)
	if err != nil {
		return err
	}

	return err
}


/*func QueryToUpdate(ssv9000005I *models.SSV9000005I)(ssv9000005O *models.SSV9000005O, err error){
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.Errorf("SV00000001",  "QueryT_pdsbasp panic:%v " , fmt.Sprintf("%s", e.(interface{})))
		}
	}()

	sql := "SELECT * FROM t_person_argeement where agreement_id=? and agreement_type=? and tcc_status='0'"

	queryInfo, err := EngineCache.QueryString(sql, ssv9000005I.AgreementID,ssv9000005I.AgreementType)
	log.Debug(queryInfo)
	if err != nil {
		return nil, err
	}

	if len(queryInfo) > 0 {
		return ssv9000005O, nil
	} else {
		err = errors.New("Records not found", "SV99000020")
		return nil, err

	}
}*/
