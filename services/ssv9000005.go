//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/sv/ssv9000005/constant"
	"git.forms.io/isaving/sv/ssv9000005/dao"
	"git.forms.io/isaving/sv/ssv9000005/models"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)

var Ssv9000005Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv9000005",
	ConfirmMethod: "ConfirmSsv9000005",
	CancelMethod:  "CancelSsv9000005",
}

type Ssv9000005 interface {
	TrySsv9000005(*models.SSV9000005I) (*models.SSV9000005O, error)
	ConfirmSsv9000005(*models.SSV9000005I) (*models.SSV9000005O, error)
	CancelSsv9000005(*models.SSV9000005I) (*models.SSV9000005O, error)
}

type Ssv9000005Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
	Sv900005O *models.SSV9000005O //SV900005O
	Sv900005I *models.SSV9000005I //SV900005I
}

// @Desc Ssv9000005 process
// @Author
// @Date 2020-12-04
func (impl *Ssv9000005Impl) TrySsv9000005(ssv9000005I *models.SSV9000005I) (ssv9000005O *models.SSV9000005O, err error) {
	log.Debug("Start try ssv9000005")

	ssv9000005O = &models.SSV9000005O{

	}
	ssv9000005O, err=dao.QueryToUpdate(ssv9000005I)
	if err != nil {
		return nil, errors.New("Query failed", constant.ERRCODE20)
	}
	return ssv9000005O, err

}

func (impl *Ssv9000005Impl) ConfirmSsv9000005(ssv9000005I *models.SSV9000005I) (ssv9000005O *models.SSV9000005O, err error) {
	log.Debug("Start confirm ssv9000005")
	err=dao.UpdateTPersonArgeement(ssv9000005I)
	//sql := "update `t_person_argeement` set accunt_nme=? ,cstmr_cntct_add=? ,cstmr_cntct_ph=? ,cstmr_cntct_em=? where agreement_id=? and agreement_type=?  and tcc_status='0'"
	//_, err = dao.EngineCache.Exec(sql,  ssv9000005I.AccuntNme, ssv9000005I.CstmrCntctAdd, ssv9000005I.CstmrCntctPh, ssv9000005I.CstmrCntctEm, ssv9000005I.AgreementID,ssv9000005I.AgreementType)
	if err != nil {
		return nil, errors.New("update failed", constant.ERRCODE20)
	}

	return nil, err
}

func (impl *Ssv9000005Impl) CancelSsv9000005(ssv9000005I *models.SSV9000005I) (ssv9000005O *models.SSV9000005O, err error) {
	log.Debug("Start cancel ssv9000005")
	return nil, nil
}
