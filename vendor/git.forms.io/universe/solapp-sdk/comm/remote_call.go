//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package comm

import (
	"encoding/json"
	"fmt"
	"git.forms.io/universe/comm-agent/client"
	common "git.forms.io/universe/comm-agent/common/protocol"
	"git.forms.io/universe/common/util"
	"git.forms.io/universe/solapp-sdk/compensable"
	"git.forms.io/universe/solapp-sdk/config"
	"git.forms.io/universe/solapp-sdk/const"
	"git.forms.io/universe/solapp-sdk/event/alert"
	"git.forms.io/universe/solapp-sdk/event/message"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego"
	jsoniter "github.com/json-iterator/go"
	"github.com/pkg/errors"
	"github.com/valyala/fasthttp"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	HTTP_METHOD_POST = "POST"
	HTTP_METHOD_GET  = "GET"
)

type Client interface {
	SyncCall(dtsCtx *compensable.TxnCtx, dstOrg, dstDcn, serviceKey string, requestMessage *client.UserMessage, replyMessage *client.UserMessage, timeoutSeconds int) error
	//invalid function name,Use new function SyncCallV2
	//SyncCall_V2(dtsCtx *compensable.TxnCtx, dstOrg, dstDcn, serviceKey string, requestMessage *client.UserMessage, replyMessage *client.UserMessage, timeoutMilliseconds int, maxRetryTimes int) error
	SyncCallV2(dtsCtx *compensable.TxnCtx, dstOrg, dstDcn, serviceKey string, requestMessage *client.UserMessage, replyMessage *client.UserMessage, timeoutMilliseconds int, maxRetryTimes int) error

	// httpMethod:POST/GET
	HttpRequest(dtsCtx *compensable.TxnCtx, httpMethod string, url string, request interface{}, requestHeader map[string]string, response interface{}, replyHeader *map[string]string, timeoutMilliseconds int) error
	ReplyTo(replyTo string, replyMessage *client.UserMessage) error

	AckQueue(ackMsgId uint64) error
	SemiSyncCall(dtsCtx *compensable.TxnCtx, dstOrg, dstDcn, serviceKey string, requestMessage *client.UserMessage, replyMessage *client.UserMessage, timeout int) error
	AsyncCall(dtsCtx *compensable.TxnCtx, dstOrg, dstDcn, serviceKey string, requestMessage *client.UserMessage) error
	Alert(level int, content string)
}

var ErrTimeout = errors.New("timeout")

func NewRemoteClientFactory() *RemoteCallClientFactory {
	return &RemoteCallClientFactory{}
}

type CommonResponse struct {
	ErrorCode int //-1 indicat error ,0 indicat success
	ErrorMsg  string
	Data      interface{}
}

var httpClient *http.Client

var once sync.Once

type ClientFactory interface {
	CreateClient() Client
}

type RemoteCallClient struct{}

type RemoteCallClientFactory struct{}

func (T *RemoteCallClientFactory) CreateClient() Client {
	once.Do(func() {
		transport := http.Transport{
			DisableKeepAlives: false,
		}
		httpClient = &http.Client{
			Transport: &transport,
		}
	})
	return &RemoteCallClient{}
}

func (T *RemoteCallClient) ReplyTo(replyTo string, replyMessage *client.UserMessage) (err error) {
	var userProps map[string]string
	if nil == replyMessage.AppProps {
		userProps = make(map[string]string)
	} else {
		userProps = replyMessage.AppProps
	}
	userProps[common.TXN_IS_SEMI_SYNC_CALL] = ""
	userProps[common.RR_REPLY_TO] = replyTo
	err = client.ReplySemiSyncCall(replyMessage)
	if nil != err {
		log.Errorf("ReplyTo.ReplySemiSyncCall failed, error=[%v]", util.ErrorToString(err))
		return err
	}

	return nil
}

func (T *RemoteCallClient) AckQueue(ackMsgId uint64) (err error) {
	err = client.SendAckToQueueMessage(ackMsgId)
	if nil != err {
		log.Errorf("ReplyTo.ReplySemiSyncCall failed, error=[%v]", util.ErrorToString(err))
		return err
	}

	return nil
}

func (T *RemoteCallClient) AsyncCall(dtsCtx *compensable.TxnCtx, dstOrg, dstDcn, serviceKey string, requestMessage *client.UserMessage) error {
	var dtsCtxMap map[string]string
	if dtsCtx != nil {
		dtsCtxMap = make(map[string]string)
		dtsCtxMap[constant.TXN_PROPAGATE_ROOT_XID_KEY] = dtsCtx.RootXid
		dtsCtxMap[constant.TXN_PROPAGATE_PARENT_XID_KEY] = dtsCtx.BranchXid
		dtsCtxMap[constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS] = dtsCtx.DtsAgentAddress
		if requestMessage.AppProps == nil {
			requestMessage.AppProps = dtsCtxMap
		} else {
			for k, v := range dtsCtxMap {
				requestMessage.AppProps[k] = v
			}
		}
	} else {
		if nil != requestMessage.AppProps {
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_ROOT_XID_KEY)
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_PARENT_XID_KEY)
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS)
		}
	}

	var err error
	if strings.EqualFold(constant.COMM_DIRECT, config.CmpSvrConfig.CommType) {
		requestTxnMessage := &message.TxnMessage{
			Id:             requestMessage.Id,
			TopicAttribute: requestMessage.TopicAttribute,
			NeedReply:      requestMessage.NeedReply,
			NeedAck:        requestMessage.NeedAck,

			//app properties
			AppProps: requestMessage.AppProps,

			//message payload
			Body: string(requestMessage.Body),
		}

		err = T.HttpPost(dtsCtx, beego.AppConfig.String(fmt.Sprintf("%s::direct", serviceKey)), requestTxnMessage, nil, nil, nil, 60)
		if nil != err {
			log.Errorf("AsyncCall.HttpPost failed, error=[%v]", util.ErrorToString(err))
			return err
		}
	} else {
		topicAttribute, err := client.BuildBussinessTopicAttributes(dstOrg, dstDcn, beego.AppConfig.String(fmt.Sprintf("%s::mesh", serviceKey)))
		if nil != err {
			log.Errorf("AsyncCall.BuildBussinessTopicAttributes failed, error=[%v]", util.ErrorToString(err))
			return err
		}

		requestMessage.TopicAttribute = topicAttribute
		err = client.PublishMessage(requestMessage)
		if nil != err {
			log.Errorf("AsyncCall.PublishMessage failed, error=[%v]", util.ErrorToString(err))
			return err
		}
	}

	return err
}

func (T *RemoteCallClient) HttpRequest(dtsCtx *compensable.TxnCtx, httpMethod string, url string, request interface{}, requestHeader map[string]string, response interface{}, replyHeader *map[string]string, timeoutMilliseconds int) (err error) {
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	if nil != response {
		if kind := reflect.ValueOf(response).Type().Kind(); kind != reflect.Ptr {
			err = errors.New("reply param should be pointer type")
			return
		}
	}

	httpClient.Timeout = time.Millisecond * time.Duration(timeoutMilliseconds)

	requestBytes, err := json.Marshal(request)
	if err != nil {
		log.Errorf("HttpPost Marshal request failed err=%v", util.ErrorToString(err))
		return
	}
	log.Debugf("http method:[%s], Request with [%s]", httpMethod, string(requestBytes))
	req, err := http.NewRequest(httpMethod, url, strings.NewReader(string(requestBytes)))
	if err != nil {
		return
	}

	for k, v := range requestHeader {
		req.Header.Add(k, v)
	}

	// to avoid app properties override dts properties!
	if dtsCtx != nil {
		req.Header.Add(constant.TXN_PROPAGATE_ROOT_XID_KEY, dtsCtx.RootXid)
		req.Header.Add(constant.TXN_PROPAGATE_PARENT_XID_KEY, dtsCtx.BranchXid)
		req.Header.Add(constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS, dtsCtx.DtsAgentAddress)
	}

	req.Header.Set("Content-Type", "application/json")

	resp, err := httpClient.Do(req)
	if err != nil {
		return
	}

	if resp.StatusCode != 200 {
		err = errors.Errorf("%d:%s", resp.StatusCode, resp.Status)
		return
	}
	if nil != response {
		defer func() { _ = resp.Body.Close() }()
		responseBody, e := ioutil.ReadAll(resp.Body)
		if e != nil {
			err = e
			//log.Errorf("Do http post failed: %s", err.Error())
			return
		}
		replyHeader := make(map[string]string)
		for k, vs := range resp.Header {
			if len(vs) > 0 {
				replyHeader[k] = vs[0]
			}
		}

		err = json.Unmarshal(responseBody, response)
	}
	return
}

func (T *RemoteCallClient) HttpPost(dtsCtx *compensable.TxnCtx, url string, request interface{}, requestHeader map[string]string, response interface{}, replyHeader *map[string]string, timeout int) (err error) {
	return T.HttpRequest(dtsCtx, "POST", url, request, requestHeader, response, replyHeader, timeout*1000)
}

func (T *RemoteCallClient) Alert(level int, content string) {

	attributes, _ := BuildAlertTopicAttributes(beego.AppConfig.String("alert::topicName"))

	if constant.ALERT_LEVEL_YELLOW != level &&
		constant.ALERT_LEVEL_ORANGE != level &&
		constant.ALERT_LEVEL_RED != level {
		log.Errorf("Alert level[%d] is invalid, please check!", level)
		return
	}

	rapmAlertEvent := alert.RapmAlertEvent{
		Level:     strconv.Itoa(level),
		Content:   content,
		AlertTime: CurrentTime(),
	}

	log.Debugf("Public alert message[%++v]", rapmAlertEvent)
	req, e := json.Marshal(rapmAlertEvent)
	if e != nil {
		log.Error("Marshal alert event failed, error=[%v]", util.ErrorToString(e))
		return
	}

	msg := &client.UserMessage{
		TopicAttribute: attributes,
		Body:           req,
	}

	e = client.PublishMessage(msg)
	if nil != e {
		log.Error("Publish message failed, error=[%v]", util.ErrorToString(e))
	}
}

func (T *RemoteCallClient) SemiSyncCall(dtsCtx *compensable.TxnCtx, dstOrg, dstDcn, serviceKey string, requestMessage *client.UserMessage, replyMessage *client.UserMessage, timeout int) error {
	var dtsCtxMap map[string]string
	if dtsCtx != nil {
		dtsCtxMap = make(map[string]string)
		dtsCtxMap[constant.TXN_PROPAGATE_ROOT_XID_KEY] = dtsCtx.RootXid
		dtsCtxMap[constant.TXN_PROPAGATE_PARENT_XID_KEY] = dtsCtx.BranchXid
		dtsCtxMap[constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS] = dtsCtx.DtsAgentAddress
		if requestMessage.AppProps == nil {
			requestMessage.AppProps = dtsCtxMap
		} else {
			for k, v := range dtsCtxMap {
				requestMessage.AppProps[k] = v
			}
		}
	} else {
		if nil != requestMessage.AppProps {
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_ROOT_XID_KEY)
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_PARENT_XID_KEY)
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS)
		}
	}

	var res *client.UserMessage
	var err error
	if strings.EqualFold(constant.COMM_DIRECT, config.CmpSvrConfig.CommType) {
		err = errors.New("Not support semi-sync call in direct mode!")
		return err
	} else {
		topicAttribute, err := client.BuildBussinessTopicAttributes(dstOrg, dstDcn, beego.AppConfig.String(fmt.Sprintf("%s::mesh", serviceKey)))
		if nil != err {
			log.Errorf("Build business topic attributes failed:Org[%s],Dcn[%s],Service key[%s]", dstOrg, dstDcn, serviceKey)
			return err
		}
		if requestMessage.AppProps == nil {
			requestMessage.AppProps = make(map[string]string)
		}
		requestMessage.AppProps[common.TXN_IS_SEMI_SYNC_CALL] = "1"

		requestMessage.TopicAttribute = topicAttribute
		res, err = client.SendRequestMessage(requestMessage)
		if nil != err {
			return err
		}
		replyMessage.Id = res.Id
		replyMessage.TopicAttribute = res.TopicAttribute
		replyMessage.AppProps = res.AppProps
		replyMessage.NeedAck = res.NeedAck
		replyMessage.NeedReply = res.NeedReply
		replyMessage.Body = res.Body
	}

	return err
}

func (T *RemoteCallClient) SyncCall(dtsCtx *compensable.TxnCtx, dstOrg, dstDcn, serviceKey string, requestMessage *client.UserMessage, replyMessage *client.UserMessage, timeout int) error {
	var dtsCtxMap map[string]string
	if dtsCtx != nil {
		dtsCtxMap = make(map[string]string)
		dtsCtxMap[constant.TXN_PROPAGATE_ROOT_XID_KEY] = dtsCtx.RootXid
		dtsCtxMap[constant.TXN_PROPAGATE_PARENT_XID_KEY] = dtsCtx.BranchXid
		dtsCtxMap[constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS] = dtsCtx.DtsAgentAddress
		if requestMessage.AppProps == nil {
			requestMessage.AppProps = dtsCtxMap
		} else {
			for k, v := range dtsCtxMap {
				requestMessage.AppProps[k] = v
			}
		}
	} else {
		if nil != requestMessage.AppProps {
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_ROOT_XID_KEY)
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_PARENT_XID_KEY)
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS)
		}
	}

	if nil != requestMessage.AppProps {
		delete(requestMessage.AppProps, common.TXN_IS_SEMI_SYNC_CALL)
	}

	var res *client.UserMessage
	var err error
	if strings.EqualFold(constant.COMM_DIRECT, config.CmpSvrConfig.CommType) {
		requestTxnMessage := &message.TxnMessage{
			Id:             requestMessage.Id,
			TopicAttribute: requestMessage.TopicAttribute,
			NeedReply:      requestMessage.NeedReply,
			NeedAck:        requestMessage.NeedAck,

			//app properties
			AppProps: requestMessage.AppProps,

			//message payload
			Body: string(requestMessage.Body),
		}

		responseTxnMessage := &message.TxnMessage{}

		err = T.HttpPost(dtsCtx, beego.AppConfig.String(fmt.Sprintf("%s::direct", serviceKey)), requestTxnMessage, nil, responseTxnMessage, nil, timeout)
		if nil != err {
			return err
		}
		replyMessage.Id = responseTxnMessage.Id
		replyMessage.TopicAttribute = responseTxnMessage.TopicAttribute
		replyMessage.AppProps = responseTxnMessage.AppProps
		replyMessage.NeedAck = responseTxnMessage.NeedAck
		replyMessage.NeedReply = responseTxnMessage.NeedReply
		replyMessage.Body = []byte(responseTxnMessage.Body)
	} else {
		topicAttribute, err := client.BuildBussinessTopicAttributes(dstOrg, dstDcn, beego.AppConfig.String(fmt.Sprintf("%s::mesh", serviceKey)))
		if nil != err {
			log.Errorf("Build bussiness topic attributes failed:Org[%s],Dcn[%s],Service key[%s]", dstOrg, dstDcn, serviceKey)
			return err
		}
		requestMessage.TopicAttribute = topicAttribute
		res, err = client.SendRequestMessage(requestMessage)
		if nil != err {
			return err
		}
		replyMessage.Id = res.Id
		replyMessage.TopicAttribute = res.TopicAttribute
		replyMessage.AppProps = res.AppProps
		replyMessage.NeedAck = res.NeedAck
		replyMessage.NeedReply = res.NeedReply
		replyMessage.Body = res.Body
	}

	return err
}

func (T *RemoteCallClient) SyncCallV2(dtsCtx *compensable.TxnCtx, dstOrg, dstDcn, serviceKey string, requestMessage *client.UserMessage, replyMessage *client.UserMessage, timeoutMilliseconds int, maxRetryTimes int) error {
	var dtsCtxMap map[string]string
	if dtsCtx != nil {
		dtsCtxMap = make(map[string]string)
		dtsCtxMap[constant.TXN_PROPAGATE_ROOT_XID_KEY] = dtsCtx.RootXid
		dtsCtxMap[constant.TXN_PROPAGATE_PARENT_XID_KEY] = dtsCtx.BranchXid
		dtsCtxMap[constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS] = dtsCtx.DtsAgentAddress
		if requestMessage.AppProps == nil {
			requestMessage.AppProps = dtsCtxMap
		} else {
			for k, v := range dtsCtxMap {
				requestMessage.AppProps[k] = v
			}
		}
	} else {
		if nil != requestMessage.AppProps {
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_ROOT_XID_KEY)
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_PARENT_XID_KEY)
			delete(requestMessage.AppProps, constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS)
		}
	}

	if nil != requestMessage.AppProps {
		delete(requestMessage.AppProps, common.TXN_IS_SEMI_SYNC_CALL)
	}

	var res *client.UserMessage
	var err error
	tryCounts := 0
	if strings.EqualFold(constant.COMM_DIRECT, config.CmpSvrConfig.CommType) {
		requestTxnMessage := &message.TxnMessage{
			Id:             requestMessage.Id,
			TopicAttribute: requestMessage.TopicAttribute,
			NeedReply:      requestMessage.NeedReply,
			NeedAck:        requestMessage.NeedAck,

			//app properties
			AppProps: requestMessage.AppProps,

			//message payload
			Body: string(requestMessage.Body),
		}

		responseTxnMessage := &message.TxnMessage{}
		url := beego.AppConfig.String(fmt.Sprintf("%s::direct", serviceKey))
		for {
			tryCounts++
			err = T.HttpRequest(dtsCtx, "POST", url, requestTxnMessage, nil, responseTxnMessage, nil, timeoutMilliseconds)
			if util.IsError(err, fasthttp.ErrTimeout) {
				if tryCounts <= maxRetryTimes {
					log.Errorf("Request url=[%s] timeout[error=%v], try counts[%d], try again...", url, util.ErrorToString(err), tryCounts)
					continue
				} else {
					log.Errorf("Request target url[%s] failed. ", url)
					return ErrTimeout
				}
			} else if nil != err {
				return err
			} else {
				break
			}
		}
		replyMessage.Id = responseTxnMessage.Id
		replyMessage.TopicAttribute = responseTxnMessage.TopicAttribute
		replyMessage.AppProps = responseTxnMessage.AppProps
		replyMessage.NeedAck = responseTxnMessage.NeedAck
		replyMessage.NeedReply = responseTxnMessage.NeedReply
		replyMessage.Body = []byte(responseTxnMessage.Body)
	} else {
		targetTopicId := beego.AppConfig.String(fmt.Sprintf("%s::mesh", serviceKey))
		topicAttribute, err := client.BuildBussinessTopicAttributes(dstOrg, dstDcn, targetTopicId)
		if nil != err {
			log.Errorf("Build bussiness topic attributes failed:Org[%s],Dcn[%s],Service key[%s]", dstOrg, dstDcn, serviceKey)
			return err
		}
		requestMessage.TopicAttribute = topicAttribute
		for {
			tryCounts++
			res, err = client.SendRequestMessageV2(requestMessage, timeoutMilliseconds)
			if util.IsError(err, fasthttp.ErrTimeout) {
				if tryCounts <= maxRetryTimes {
					log.Errorf("Request target topic id=[%s] timeout[error=%v], try counts[%d], try again...", targetTopicId, util.ErrorToString(err), tryCounts)
					continue
				} else {
					log.Errorf("Request target topic id[%s] failed. ", targetTopicId)
					return ErrTimeout
				}
			} else if nil != err {
				return err
			} else {
				break
			}
		}
		replyMessage.Id = res.Id
		replyMessage.TopicAttribute = res.TopicAttribute
		replyMessage.AppProps = res.AppProps
		replyMessage.NeedAck = res.NeedAck
		replyMessage.NeedReply = res.NeedReply
		replyMessage.Body = res.Body
	}

	return err
}

func (T *RemoteCallClient) DTSRequestWithEvent(dtsCtx *compensable.TxnCtx, dstOrg, dstDcn, nodeId, instanceId, topicId string, event interface{}, reply interface{}, timeout int) (err error) {
	if nil != reply {
		if kind := reflect.ValueOf(reply).Type().Kind(); kind != reflect.Ptr {
			err = errors.New("reply param should be pointer type")
			return
		}
	}

	requestBytes, err := json.Marshal(event)
	if err != nil {
		log.Errorf("DTSRequestWithEvent Marshal request failed err=%v", err)
		return
	}

	attributes, err := BuildDTSTopicAttributes(dstOrg, dstDcn, nodeId, instanceId, topicId)
	if nil != err {
		log.Errorf("Build DTS topic attributes failed:Org[%s],Dcn[%s],NodeId[%s], InstanceId[%s],TopicId[%s]", dstOrg, dstDcn, nodeId, instanceId, topicId)
		return err
	}

	msg := &client.UserMessage{
		TopicAttribute: attributes,
		Body:           requestBytes,
	}
	ret, err := client.SendRequestMessage(msg)

	if nil != err {
		return
	}

	err = json.Unmarshal(ret.Body, reply)

	return
}

func BuildAlertTopicAttributes(dstTopicId string) (map[string]string, error) {
	attributeMap := make(map[string]string)
	attributeMap[common.TOPIC_TYPE] = common.TOPIC_TYPE_ALERT
	attributeMap[common.TOPIC_ID] = dstTopicId

	return attributeMap, nil
}

//func BuildDTSTopicAttributes(srcNodeId, srcInsId, dstOrg, dstDcn, nodeId, instanceId, dstTopicId string) (map[string]string, error) {
func BuildDTSTopicAttributes(dstOrg, dstDcn, nodeId, instanceId, dstTopicId string) (map[string]string, error) {
	attributeMap := make(map[string]string)
	if dstOrg == "" || dstDcn == "" || dstTopicId == "" {
		return attributeMap, fmt.Errorf("dstOrg=%s,dstDcn=%s,dstTopicId=%s have empty field", dstOrg, dstDcn, dstTopicId)
	}

	attributeMap[common.TOPIC_TYPE] = common.TOPIC_TYPE_DTS
	//attributeMap[common.TOPIC_SOURCE_NODE_ID] = srcNodeId
	//attributeMap[common.TOPIC_SOURCE_INSTANCE_ID] = srcInsId
	attributeMap[common.TOPIC_DESTINATION_ORG] = dstOrg
	attributeMap[common.TOPIC_DESTINATION_DCN] = dstDcn
	attributeMap[common.TOPIC_DESTINATION_NODE_ID] = nodeId
	attributeMap[common.TOPIC_DESTINATION_INSTANCE_ID] = instanceId
	attributeMap[common.TOPIC_ID] = dstTopicId

	return attributeMap, nil
}

func CurrentTime() string {
	t := time.Now()
	return fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d.%d",
		//t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), strconv.Itoa(t.Nanosecond())[0:3])
		t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), t.Nanosecond())
}
